import { ApplicationError } from './ApplicationError';

export class BadRequestError extends ApplicationError {
  constructor(systemCode: number, message?: string) {
    super(400, message || 'Bad request error.', systemCode);
  }
}