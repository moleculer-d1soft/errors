import { ApplicationError } from './ApplicationError';

export class ForbiddenError extends ApplicationError {
  constructor(systemCode: number, message?: string) {
    super(403, message || 'Forbidden error.', systemCode);
  }
}