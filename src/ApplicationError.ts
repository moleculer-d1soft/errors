import { Errors } from 'moleculer';

export class ApplicationError extends Errors.MoleculerError {
  public readonly systemCode?: number;

  constructor(httpCode: number, message?: string, systemCode?: number) {
    super(message || 'error', httpCode);
    this.type = systemCode ? systemCode.toString() : '10001';
    this.systemCode = systemCode;
  }
}