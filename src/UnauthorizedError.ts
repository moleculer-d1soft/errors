import { ApplicationError } from './ApplicationError';

export class UnauthorizedError extends ApplicationError {
  constructor(systemCode: number, message?: string) {
    super(401, message, systemCode);
  }
}