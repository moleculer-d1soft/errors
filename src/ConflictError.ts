import { ApplicationError } from './ApplicationError';

export class ConflictError extends ApplicationError {
  constructor(systemCode: number, message?: string) {
    super(409, message, systemCode);
  }
}