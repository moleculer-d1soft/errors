import { ApplicationError } from './ApplicationError';

export function isApplicationError(error: unknown | ApplicationError): error is ApplicationError {
    return !!(<ApplicationError>error).systemCode;
}