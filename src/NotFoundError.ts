import { ApplicationError } from './ApplicationError';

export class NotFoundError extends ApplicationError {
  constructor(systemCode: number, message?: string) {
    super(404, message || 'Not found error.', systemCode);
  }
}