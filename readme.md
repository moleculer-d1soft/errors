# Moleculer d1soft / Errors

Base application errors.  
All errors relates to specified http code.  
All application errors extends from this base errors.  

## Install

```sh
npm add -E @moleculer-d1soft/errors
```

## Example

```ts
import { NotFoundError } from '@bukusaya/errors';

export enum ErrorCodes {
  UserNotFound = 101,
}

export class UserNotFound extends NotFoundError {
  constructor(userId: string) {
    super(ErrorCodes.UserNotFound, `User with id ${userId} not found`);
  }
}

// next in user's related code

const user = await this.usersRepository.getById(userId);
if (!user) {
  throw new UserNotFound(userId);
}

```